Attribute VB_Name = "LovdataScraper"
Option Private Module


Sub ScrapeWithHTMLObj(url As String, domClassName As String, domTag As String, domAttribute As String, domAttributeValue As String)
    ' Dependencies:
    ' * Microsoft HTML Object Library

    ' Declare vars
    Dim pHTML As HTMLDocument
    Dim pElements As Object, pElement As Object

    Set pHTML = New HTMLDocument

    ' Basic URL healthcheck
    Do While (url = "" Or (Left(url, 7) <> "http://" And Left(url, 8) <> "https://"))
        MsgBox ("Invalid URL!")
        url = InputBox("Enter new URL: (0 to terminate)")
        If url = "0" Then Exit Sub
    Loop

    ' Fetch page at URL
    With CreateObject("WINHTTP.WinHTTPRequest.5.1")
        .Open "GET", url, False
        .Send
        pHTML.body.innerHTML = .ResponseText
    End With

    ' Declare page elements
    Set pElements = pHTML.getElementsByClassName(domClassName)
    Set pElement = pElements(0).getElementsByTagName(domTag)

    ' Extract only elements with wanted attribute
    pEleArray = getElementsByAttribute(pElement, domAttribute, domAttributeValue)

    For Each e In pEleArray
        ' Do stuff to elements
        Debug.Print e.getAttribute(domAttribute)
    Next
End Sub


Function getElementsByAttribute(pObj As Object, domAttribute As String, domAttributeValue As String) As Object()
    Dim oTemp() As Object
    ReDim oTemp(1 To 1)

    For i = 0 To pObj.Length - 1
        If pObj(i).getAttribute(domAttribute) = domAttributeValue Then
            Set oTemp(UBound(oTemp)) = pObj(i)
            ReDim Preserve oTemp(1 To UBound(oTemp) + 1)
        End If
    Next i

    ReDim Preserve oTemp(1 To UBound(oTemp) - 1)

    getElementsByAttribute = oTemp
End Function
