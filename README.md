# README #

A very basic webscraper, customized for lovdata.no (legal text), taking the URL to a specific law as well as which structure of classes and tags to iterate over as its arguments.

Example usage:
Call ScrapeWithHTMLObj("https://lovdata.no/dokument/SF/forskrift/2004-01-19-298", "chapter-index", "div", "data-level", "1")